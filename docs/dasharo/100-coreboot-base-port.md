# Dasharo: coreboot base port

## Test cases

### CBP001.001 Firmware building

Refer to [coreboot building documentation](../hardware/clevo-nv41/building.md)

### CBP002.001 Firmware flashing - external programmer

Refer to [coreboot external flashing documentation](../hardware/clevo-nv41/flashing_external.md)

### CBP003.001 Firmware flashing - internal programmer (Ubuntu 20.04)

Refer to [coreboot internal flashing documentation](../hardware/clevo-nv41/flashing_internal.md)
