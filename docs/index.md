<img src="images/logo/logo-bar.svg" width="1000" height="500">

## About

A new kind of BIOS firmware that will solve the problems of ownership, security
and compatibility allowing you create secure, efficient images that are fully
customizable to your tastes and needs.

Dasharo is a set of productized services, Open Core, and SaaS products which
help to provide scalable, modular, easy to combine Open Source BIOS, UEFI, and
Firmware solutions. It offers the components that are needed to develop and
maintain a high quality, and modular firmware, for the stability and security
of your platform.

[Dasharo homepage](https://dasharo.com/)
