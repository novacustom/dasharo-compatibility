# Dasharo Compatibility Specification

* [Generic test setup](generic-test-setup.md)
* [Memory HCL](301-memory-hcl.md)
* [Custom boot menu key](303-custom-key.md)
* [Custom logo](304-custom-logo.md)
* [USB HID and MSC support](306-usb-hid-and-msc-support.md)
* [Debian Stable and Ubuntu LTS support](308-debian-stable-and-ubuntu-lts-support.md)
* [UEFI compatible interface](30M-uefi-compatible-interface.md)
* [UEFI Shell](30P-uefi-shell.md)
* [NVMe support](312-nvme-support.md)
* [Windows 10 booting](31A-windows-10-booting.md)
* [Display ports and LCD support](31E-display-ports-and-lcd.md)
* [Embedded Controller and Super I/O initialization](31G-ec-and-superio.md)
