# Dasharo Compatibility: xxx

## Test cases

### xxx001.001 xxx

**Test description**

xxx

**Test configuration data**

1. `FIRMWARE` = coreboot
1. `BOOT_MENU_KEY` = `F7`

**Test setup**

1. Proceed with the
   [Generic test setup: firmware](../generic-test-setup/#firmware)

**Test steps**

xxx

**Expected result**

xxx
