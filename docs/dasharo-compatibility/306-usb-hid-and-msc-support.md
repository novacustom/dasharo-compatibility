# Dasharo Compatibility: USB HID and MSC Support

## Test cases

### Common

**Test setup**

1. Proceed with the
   [Generic test setup: firmware](../generic-test-setup/#firmware)
1. Proceed with the
   [Generic test setup: OS installer](../generic-test-setup/#os-installer)
1. Proceed with the
   [Generic test setup: OS installation](../generic-test-setup/#os-installation)
1. Proceed with the
   [Generic test setup: OS boot from disk](../generic-test-setup/#os-boot-from-disk)

### USB001.001 USB devices detected in FW

**Test description**

This test verifies that the external USB devices are detected correctly
by the firmware and all basic keys work according to their labels.

**Test configuration data**

1. `FIRMWARE` = coreboot
1. `BIOS_SETUP_KEY` = `F2`

**Test setup**

1. Proceed with the
   [Generic test setup: firmware](../generic-test-setup/#firmware)
1. Proceed with the
   [Generic test setup: OS installer](../generic-test-setup/#os-installer)
1. Connect the flash drive using the USB port

**Test steps**

1. Power ON the DUT
1. Enter the boot menu using the `BIOS_SETUP_KEY`
1. Select the `Boot Menu`, press `Enter` and note the result

**Expected result**

1. Flash drive entry is listed in the boot menu

### USB001.002 USB devices detected by OS (Ubuntu 20.04)

**Test description**

This test verifies that the external USB devices are detected correctly
by the `OPERATING_SYSTEM` and all basic keys work according to their labels.

**Test configuration data**

1. `FIRMWARE` = coreboot
1. `BIOS_SETUP_KEY` = `F2`
1. `OPERATING_SYSTEM` = Ubuntu 20.04


**Test setup**

1. Proceed with the [Common](#common) section.

**Test steps**

1. Power ON the DUT
1. Open the terminal, run the command: `watch -n1 lsusb`
1. Connect external USB devices to DUT USB A port and note the result

**Expected result**

1. After each device is connected to the USB port, a new USB device entry
   in `lsusb` command output should appear

### USB001.003 USB devices detected by OS (Windows 10)

**Test description**

This test verifies that the external USB devices are detected correctly
by the `OPERATING_SYSTEM` and all basic keys work according to their labels.

**Test configuration data**

1. `FIRMWARE` = coreboot
1. `BIOS_SETUP_KEY` = `F2`
1. `OPERATING_SYSTEM` = Windows 10

**Test setup**

1. Proceed with the [Common](#common) section.

**Test steps**

1. Power ON the DUT
1. Open PowerShell and execute the command:
   `while (1) {Get-PnpDevice -PresentOnly | Where-Object { $_.InstanceId -match '^USB' }; sleep 5; clear}`
1. Connect external USB devices to DUT USB A port and note the result

**Expected result**

1. After each device is connected to the USB port, a new USB device entry in the
   output of the command should appear

### USB002.001 USB keyboard detected in FW

**Test description**

This test verifies that the external USB keyboard is detected correctly
by the firmware and all basic keys work according to their labels.

**Test configuration data**

1. `FIRMWARE` = coreboot
1. `BIOS_SETUP_KEY` = `F2`

**Test setup**

1. Proceed with the
   [Generic test setup: firmware](../generic-test-setup/#firmware)
1. Connect the external USB keyboard using the USB port

**Test steps**

1. Power on the DUT and press the `BIOS_SETUP_KEY` to enter the setup menu
1. Use the arrow keys, Esc key and the Enter key to navigate the menus

**Expected result**

1. All menus can be entered using the external USB keyboard

### USB002.002 USB keyboard in OS (Ubuntu 20.04)

**Test description**

This test verifies that the external USB keyboard is detected correctly
by the `OPERATING_SYSTEM` and all basic keys work according to their labels.

**Test configuration data**

1. `FIRMWARE` = coreboot
1. `BIOS_SETUP_KEY` = `F2`
1. `OPERATING_SYSTEM` = Ubuntu 20.04

**Test setup**

1. Proceed with the [Common](#common) section.
1. Install `libinput-tools` on the DUT.
1. Connect the external USB keyboard using the USB port.

**Test steps**

1. Run `lsusb` in the terminal.
1. Run `sudo libinput debug-events --show-keycodes` in the terminal:
    1. Test the alphanumeric keys and note the generated keycodes
    1. Test non-alphanumeric keys and verify that they generate the correct
       keycodes
    1. Test key combinations with the `Shift`, `Ctrl` and `Alt` modifier keys
       (this tests 2-key rollover)

**Expected result**

1. The extarnal USB keyboard is detected in OS.
1. All standard keyboard keys generate the correct keycodes and events as per
   their labels
1. Key combinations are detected correctly

### USB002.003 USB keyboard in OS (Windows 10)

**Test description**

This test verifies that the external USB keyboard is detected correctly
by the `OPERATING_SYSTEM` and all basic keys work according to their labels.

**Test configuration data**

1. `FIRMWARE` = coreboot
1. `BIOS_SETUP_KEY` = `F2`
1. `OPERATING_SYSTEM` = Windows 10

**Test setup**

1. Proceed with the [Common](#common) section.
1. Connect the external USB keyboard using the USB port.

**Test steps**

1. Open `notepad`
    1. Test the alphanumeric keys and note the generated characters
    1. Test non-alphanumeric keys and verify that they generate the signs
    1. Test key combinations with the `Shift`, and `Alt` modifier keys
1. Open `On-Screen Keyboard` and press `Ctrl` key on the hardware keyboard.
   Check if `On-Screen Keyboard` correctly highlights it.
1. Open `Start menu` and press `Esc`. Check if `Start menu` is properly closed.

**Expected result**

1. All standard keyboard keys generate correct characters
   or actions when pressed.
1. Key combinations are detected correctly.
