[NVCP10000] Dasharo: coreboot base port

Test cases:

1. Build
2. External flashing
3. Boot into OS (Ubuntu 20.04)

[NVCP31E00] Dasharo Compatibility: Display ports and LCD support

1. Internal LCD in firmware
2. Internal LCD in OS (Ubuntu 20.04)
3. Internal LCD in OS (Windows 10)
4. External HDMI display in OS (Ubuntu 20.04)
5. External HDMI display in OS (Windows 10)

[NVCP31G00] Dasharo Compatibility: Embedded Controller and Super I/O initialization

1. Battery monitoring - charge level
2. Battery monitoring - charging state
3. Touchpad in OS - (Ubuntu 20.04)
3. Touchpad in OS - (Windows 10)
Keyboard (standard keypad) in firmware
Keyboard (standard keypad) in OS (Ubuntu 20.04)
Keyboard (standard keypad) in OS (Windows 10)
Keyboard (function key: play/pause) in OS (Ubuntu)
Keyboard (function key: touchpad on/off) in OS (Ubuntu)
Keyboard (function key: display on/off) in OS (Ubuntu)
Keyboard (function key: mute) in OS (Ubuntu)
Keyboard (function key: keyboard backlight) in OS (Ubuntu)
Keyboard (function key: volume down) in OS (Ubuntu)
Keyboard (function key: volume up) in OS (Ubuntu)
Keyboard (function key: display switch) in OS (Ubuntu)
Keyboard (function key: brightness down) in OS (Ubuntu)
Keyboard (function key: brightness up) in OS (Ubuntu)
Keyboard (function key: camera on/off) in OS (Ubuntu)
Keyboard (function key: flight mode) in OS (Ubuntu)
Keyboard (function key: sleep) in OS (Ubuntu)


[NVCP30100] Dasharo Compatibility: Memory HCL
  - TODO: manual procedures

[NVCP30M00] Dasharo Compatibility: UEFI compatible interface
3. Boot into OS (Ubuntu 20.04)
3. Boot into OS (Windows 10)
