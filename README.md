# About

This repository contains source code for Dasharo documentation webpage

# Local build

```
$ virtualenv -p $(which python3) venv
$ source venv/bin/activate
$ pip install mkdocs mkdocs-material
$ mkdocs build
```
