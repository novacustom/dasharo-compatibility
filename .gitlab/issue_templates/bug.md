**Dasharo version**
<!--(The version of Dasharo you're using (e.g., `v0.2.0`))-->


**Hardware details**
<!--(The exact hardware model you're using (e.g., `Clevo NV41MZ`))-->


**Affected component(s) or functionality**
<!--(The component or functionality of Dasharo that is not working as expected.)-->


**Brief summary**
<!--(A clear and concise summary of the bug.)-->


**How reproducible**
<!--(At what rate does the bug occur when the steps to reproduce are performed?)-->


**How to reproduce**

Steps to reproduce the behavior:
1. 
2. 
3. 

**Expected behavior**
<!--(A clear and concise description of what you expected to happen.)-->


**Actual behavior**
<!--(What actually happened instead of what you expected to happen.)-->


**Screenshots**
<!--(If applicable, add screenshots to help explain your problem.)-->


**Additional context**
<!--(Add any other context about the problem here.)-->


**Solutions you've tried**
<!--(If applicable, any solutions or workarounds you've already tried.)-->

/label ~bug ~needs-investigation
/cc @macpijan
/assign @mkopec1
